/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples;

import java.util.Arrays;
import java.util.List;

import org.openbravo.base.cxf.SOAPWebService;
import org.openbravo.base.cxf.SOAPWebServiceRegister;
import org.openbravo.base.provider.OBSingleton;

/**
 * This class registers the {@link Calculator} service on server startup.
 */
public class SOAPExampleWebServiceRegister implements SOAPWebServiceRegister {

  @Override
  public List<SOAPWebService<? extends OBSingleton>> getSOAPWebServices() {
    return Arrays
        .asList(SOAPWebService.createSOAPWebService("calculatorService", CalculatorImpl.class));
  }
}
