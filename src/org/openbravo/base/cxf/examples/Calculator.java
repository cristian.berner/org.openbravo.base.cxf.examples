/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

/**
 * This class is a service endpoint interface (SEI). It is the piece of Java code that is shared
 * between a service and the consumers that make requests on it. The methods defined in this class
 * are intended to be mapped to the operations exposed by the service.
 */
@WebService
public interface Calculator {

  @WebMethod
  public int sum(@WebParam(name = "sum1") @XmlElement(required = true) int sum1,
      @WebParam(name = "sum2") @XmlElement(required = true) int sum2);

  @WebMethod
  public int multiply(@WebParam(name = "mul1") @XmlElement(required = true) int mul1,
      @WebParam(name = "mul2") @XmlElement(required = true) int mul2);
}
