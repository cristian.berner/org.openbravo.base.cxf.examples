/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples;

import javax.jws.WebService;

import org.openbravo.base.provider.OBSingleton;

/**
 * This class is the service implementation of the {@link Calculator} class.
 */
@WebService(endpointInterface = "org.openbravo.base.cxf.examples.Calculator", serviceName = "calculatorService", portName = "calculatorServicePort")
public class CalculatorImpl implements Calculator, OBSingleton {

  @Override
  public int sum(int sum1, int sum2) {
    return sum1 + sum2;
  }

  @Override
  public int multiply(int mul1, int mul2) {
    return mul1 * mul2;
  }

}
