/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples.test.client;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.openbravo.base.cxf.client.SOAPClient;
import org.openbravo.base.cxf.examples.gen.Calculator;
import org.openbravo.base.cxf.examples.gen.CalculatorService;
import org.openbravo.base.exception.OBException;

/**
 * A test SOAPClient to consume the "Calculator" SOAP service.
 */
public class CalculatorClient extends SOAPClient<Calculator> {

  private static final String TARGET_NAMESPACE = "http://examples.cxf.base.openbravo.org/";
  private static final String SERVICE_NAME = "calculatorService";

  public static CalculatorClient fromSEI() {
    return new CalculatorClient();
  }

  public static CalculatorClient fromImplementation() {
    return new CalculatorClient(new CalculatorService().getCalculatorServicePort());
  }

  private CalculatorClient() {
    super(Calculator.class, getWSDL(), TARGET_NAMESPACE, SERVICE_NAME);
    configure();
  }

  private CalculatorClient(Calculator implementation) {
    super(implementation);
    configure();
  }

  private static String getWSDL() {
    try {
      return CalculatorClient.class.getResource("../../../../../../../calculator.wsdl")
          .toURI()
          .toString();
    } catch (Exception ex) {
      throw new OBException("Could not retrieve WSDL" + ex.getMessage());
    }
  }

  private void configure() {
    Map<String, Object> requestData = new HashMap<>();
    requestData.put(BindingProvider.USERNAME_PROPERTY, "Openbravo");
    requestData.put(BindingProvider.PASSWORD_PROPERTY, "openbravo");
    setRequestData(requestData);
  }

}
