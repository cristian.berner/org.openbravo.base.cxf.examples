/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples.test.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.session.OBPropertiesProvider;

/**
 * This class is used to adapt a WSDL document to the environment where the tests are executed.
 */
public class WSDLCreator {

  private static final Logger log = LogManager.getLogger();
  private static final String WSDL_DIR = "/wsdl/";

  private String templateFileName;
  private String targetFileName;

  public WSDLCreator(String templateFileName, String targetFileName) {
    this.templateFileName = templateFileName;
    this.targetFileName = targetFileName;
  }

  public void generateWSDL() throws IOException {
    Optional<String> optWsdl = getWSDLContent();
    if (!optWsdl.isPresent()) {
      throw new OBException("Could not generate WSDL content from " + templateFileName);
    }
    String wsdl = optWsdl.get();
    log.debug("WSDL = {}", wsdl);
    FileUtils.writeStringToFile(getTargetFile(), wsdl);
  }

  private Optional<String> getWSDLContent() {
    try {
      URL url = getClass().getResource(WSDL_DIR + templateFileName);
      String wsdlContent = FileUtils.readFileToString(new File(url.toURI()));
      wsdlContent = wsdlContent.replace("$context.url",
          OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("context.url"));
      return Optional.of(wsdlContent);
    } catch (IOException | URISyntaxException e) {
      return Optional.empty();
    }
  }

  private File getTargetFile() {
    String sourcePath = OBPropertiesProvider.getInstance()
        .getOpenbravoProperties()
        .getProperty("source.path");
    return Paths.get(sourcePath, "src-test", "build", "classes", targetFileName).toFile();
  }

  public void deleteWSDLFile() throws IOException {
    File wsdlFile = getTargetFile();
    if (wsdlFile.exists()) {
      Files.delete(wsdlFile.toPath());
    }
  }
}
