/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;

import org.junit.Test;
import org.openbravo.base.cxf.examples.gen.Calculator;
import org.openbravo.base.cxf.examples.gen.CalculatorService;

/**
 * Tests for covering the default base HTTP Authentication mechanism of the published SOAP services.
 */
public class HttpBasicAuthenticationTest extends SOAPBaseTest {

  /**
   * Check that HttpUnauthorizedException is correctly thrown.
   */
  @Test
  public void httpUnauthorizedExceptionIsThrown() {
    // Create the service instance
    CalculatorService service = new CalculatorService();
    Calculator calculator = service.getCalculatorServicePort();

    String message = "";
    try {
      calculator.sum(1, 2);
    } catch (WebServiceException ex) {
      message = ex.getCause().getMessage();
    }

    assertThat(message, equalTo("Missing Credentials"));
  }

  /**
   * Check that HttpForbiddenException is correctly thrown.
   */
  @Test
  public void httpForbiddenExceptionIsThrown() {
    // Create the service instance
    CalculatorService service = new CalculatorService();
    Calculator calculator = service.getCalculatorServicePort();

    // Set wrong credentials for the basic HTTP authentication
    BindingProvider bindingProvider = (BindingProvider) calculator;
    Map<String, Object> requestContext = bindingProvider.getRequestContext();
    requestContext.put(BindingProvider.USERNAME_PROPERTY, "");
    requestContext.put(BindingProvider.PASSWORD_PROPERTY, "");

    String message = "";
    try {
      calculator.sum(1, 2);
    } catch (WebServiceException ex) {
      message = ex.getCause().getMessage();
    }

    assertThat(message, equalTo("Invalid Credentials"));
  }

}
