/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.junit.Test;
import org.openbravo.base.cxf.examples.gen.Calculator;
import org.openbravo.base.cxf.examples.gen.CalculatorService;
import org.openbravo.base.cxf.examples.test.client.CalculatorClient;

/**
 * This tests that it is possible to consume the "Calculator" SOAP service exposed with this module.
 */
public class SOAPConsumerTest extends SOAPBaseTest {

  /**
   * Consume the service by directly using the service generated from the WSDL.
   */
  @Test
  public void consumeWithGeneratedService() {
    // Create the service instance
    CalculatorService service = new CalculatorService();
    Calculator calculator = service.getCalculatorServicePort();

    // Set the credentials for the basic HTTP authentication
    BindingProvider bindingProvider = (BindingProvider) calculator;
    Map<String, Object> requestContext = bindingProvider.getRequestContext();
    requestContext.put(BindingProvider.USERNAME_PROPERTY, "Openbravo");
    requestContext.put(BindingProvider.PASSWORD_PROPERTY, "openbravo");

    // Print the results
    assertThat(calculator.sum(1, 2), equalTo(3));
    assertThat(calculator.multiply(1, 2), equalTo(2));
  }

  /**
   * Consume the service using the SOAPClient API (Using a client defined from the SEI).
   */
  @Test
  public void consumeWithSEIBasedSOAPClient() {
    // Create the service instance
    CalculatorClient calculator = CalculatorClient.fromSEI();

    // Print the results
    assertThat(calculator.getService().sum(1, 2), equalTo(3));
    assertThat(calculator.getService().multiply(1, 2), equalTo(2));
  }

  /**
   * Consume the service using the SOAPClient API (Using a client defined from the service
   * implementation).
   */
  @Test
  public void consumeWithImplementationBasedSOAPClient() {
    // Create the service instance
    CalculatorClient calculator = CalculatorClient.fromImplementation();

    // Print the results
    assertThat(calculator.getService().sum(1, 2), equalTo(3));
    assertThat(calculator.getService().multiply(1, 2), equalTo(2));
  }
}
