/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.examples.test;

import java.io.IOException;

import org.apache.cxf.staxutils.StaxUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openbravo.base.cxf.examples.test.util.WSDLCreator;

/**
 * This tests that it is possible to consume the "Calculator" SOAP service exposed with this module.
 */
public abstract class SOAPBaseTest {

  private static WSDLCreator creator = new WSDLCreator("calculator.wsdl.template",
      "calculator.wsdl");

  SOAPBaseTest() {
  }

  @BeforeClass
  public static void createWSDL() throws IOException {
    // Temporary workaround to make the test work when executing it with ant
    // In that case the wstx-asl-3.0.2.jar library is used which is not the minimum woodstox version
    // suggested (4.2.0)
    System.getProperties().put(StaxUtils.ALLOW_INSECURE_PARSER, "1");
    creator.generateWSDL();
  }

  @AfterClass
  public static void deleteWSDL() throws IOException {
    System.getProperties().put(StaxUtils.ALLOW_INSECURE_PARSER, "");
    creator.deleteWSDLFile();
  }
}
