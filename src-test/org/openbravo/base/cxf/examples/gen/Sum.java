
package org.openbravo.base.cxf.examples.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sum"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sum1" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sum2" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sum", propOrder = {
    "sum1",
    "sum2"
})
public class Sum {

    protected int sum1;
    protected int sum2;

    /**
     * Gets the value of the sum1 property.
     * 
     */
    public int getSum1() {
        return sum1;
    }

    /**
     * Sets the value of the sum1 property.
     * 
     */
    public void setSum1(int value) {
        this.sum1 = value;
    }

    /**
     * Gets the value of the sum2 property.
     * 
     */
    public int getSum2() {
        return sum2;
    }

    /**
     * Sets the value of the sum2 property.
     * 
     */
    public void setSum2(int value) {
        this.sum2 = value;
    }

}
