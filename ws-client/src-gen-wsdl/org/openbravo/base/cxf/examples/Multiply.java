
package org.openbravo.base.cxf.examples;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for multiply complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="multiply"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mul1" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="mul2" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multiply", propOrder = {
    "mul1",
    "mul2"
})
public class Multiply {

    protected int mul1;
    protected int mul2;

    /**
     * Gets the value of the mul1 property.
     * 
     */
    public int getMul1() {
        return mul1;
    }

    /**
     * Sets the value of the mul1 property.
     * 
     */
    public void setMul1(int value) {
        this.mul1 = value;
    }

    /**
     * Gets the value of the mul2 property.
     * 
     */
    public int getMul2() {
        return mul2;
    }

    /**
     * Sets the value of the mul2 property.
     * 
     */
    public void setMul2(int value) {
        this.mul2 = value;
    }

}
